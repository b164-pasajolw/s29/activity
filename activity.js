const express = require('express');

const app = express();

const port = 4000;


app.use(express.json());
app.use(express.urlencoded({ extended: true }));


//Mock Database
let users =[]


//no.1 & 2
app.get("/home", (req, res) => {
	res.send("Welcome to the home page")
});


//no.3 & 4
app.get("/users", (req, res) => {
	users.push(req.body)
	res.send(req.body)
})


//no.5 & 6
app.delete("/delete-user", (req, res) => {
	
	let message;
	for (let i = 0; i < users.length; i++){	
		if(req.body.firstName === users[i].firstName){
			
			users[i].password = req.body.password
		
			message = `User ${req.body.firstName}'s has been deleted`;

			break;

		} else {
			message = "User does not exist"
		}
	}
		res.send(message);
})



app.listen(port, () => console.log(`Server running at port: ${port}`));

